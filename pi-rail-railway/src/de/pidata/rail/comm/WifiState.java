package de.pidata.rail.comm;

public enum WifiState {
  green,
  yellow,
  red,
  green_bad,
  green_ok,
  green_good,
  green_top
}
