/*
 * This file is part of PI-Rail base (https://gitlab.com/pi-rail/pi-rail-base).
 * Copyright (C) 2013-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.rail.comm;

import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.models.types.simple.BinaryBytes;
import de.pidata.models.xml.binder.XmlReader;
import de.pidata.qnames.QName;
import de.pidata.rail.railway.RailDevice;
import de.pidata.rail.railway.RailDeviceAddress;
import de.pidata.stream.StreamHelper;

import java.io.*;
import java.net.*;

public class ConfigLoader implements Runnable {

  public static final String CFG_XML = "cfg.xml";
  public static final String IO_CFG_XML = "ioCfg.xml";
  public static final String NET_CFG_XML = "netCfg.xml";
  public static final String TRACK_CFG_XML = "trackCfg.xml";
  public static final String ICON_JPG = "icon.jpg";

  private boolean active = false;
  private Thread loaderThread;
  private InetAddress railDeviceAddress = null;
  private QName railDeviceID = null;
  private String filename;
  private Model configModel;
  private Model trackConfigModel;
  private Binary icon;
  private ConfigLoaderListener listener;
  private boolean tryIcon;
  private boolean tryTrackCfg;

  protected ConfigLoader( QName railDeviceId, InetAddress railDeviceAddress, String filename, ConfigLoaderListener listener, boolean tryIcon, boolean tryTrackCfg ) {
    this.railDeviceID = railDeviceId;
    this.railDeviceAddress = railDeviceAddress;
    this.filename = filename;
    this.listener = listener;
    this.tryIcon = tryIcon;
    this.tryTrackCfg = tryTrackCfg;
    this.loaderThread = new Thread( this, "Config-Loader" );
    this.loaderThread.start();
  }

  public InetAddress getRailDeviceAddress() {
    return railDeviceAddress;
  }

  public QName getRailDeviceID() {
    return railDeviceID;
  }

  public Model getConfigModel() {
    return configModel;
  }

  public Binary getIcon() {
    return icon;
  }

  public Model getTrackConfigModel() {
    return trackConfigModel;
  }

  private byte[] doDownload( RailDevice railDevice, String sourceName ) {
    RailDeviceAddress addr = railDevice.getAddress();
    if (addr != null) {
      InetAddress inetAddress = addr.getInetAddress();
      if (inetAddress != null) {
        String url = "http://" + inetAddress.getHostAddress() + "/" + sourceName;
        URI uri = URI.create( url );
        HttpURLConnection httpConnection = null;
        InputStream inStream = null;
        try {
          XmlReader xmlReader = new XmlReader();
          URL destination = uri.toURL();
          httpConnection = (HttpURLConnection) destination.openConnection();
          httpConnection.setRequestMethod( "GET" );
          inStream = httpConnection.getInputStream();
          return StreamHelper.toBytes( inStream );
        }
        catch (IOException e) {
          e.printStackTrace();
        }
        finally {
          StreamHelper.close( inStream );
          if (httpConnection != null) {
            httpConnection.disconnect();
          }
        }
      }
    }
    return null;
  }

  public Binary loadBitmap( InetAddress inetAddress, String sourceName ) {
    if (inetAddress != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/" + sourceName;
      URI uri = URI.create( url );
      InputStream inStream = null;
      HttpURLConnection httpConnection = null;
      try {
        URL destination = uri.toURL();
        httpConnection = (HttpURLConnection) destination.openConnection();
        httpConnection.setRequestMethod( "GET" );
        inStream = httpConnection.getInputStream();
        BinaryBytes bitmap = new BinaryBytes( StreamHelper.toBytes( inStream ) );
        return bitmap;
      }
      catch (FileNotFoundException e) {
        Logger.warn( "File not found, URL="+url );
      }
      catch (IOException e) {
        Logger.error( "Error loading file from URL="+url, e );
      }
      finally {
        StreamHelper.close( inStream );
        if (httpConnection != null) {
          httpConnection.disconnect();
        }
      }
    }
    return null;
  }

  public static int doUpload( InetAddress railDeviceAddress, CharSequence data, String targetFileName ) throws IOException {
    return doUpload( railDeviceAddress, data, null, targetFileName );
  }

  public static int doUpload( InetAddress railDeviceAddress, File file, String targetFileName ) throws IOException {
    return doUpload( railDeviceAddress, null, file, targetFileName );
  }

  protected static int doUpload( InetAddress inetAddress, CharSequence data, File file, String targetFileName ) throws IOException {
    int responseCode = -1;

    if (inetAddress != null && ( data != null || file != null )  && targetFileName != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/fileUpload";

      final String boundary = Long.toHexString( System.currentTimeMillis() ); // Just generate some unique random value.
      final String charset = "UTF-8";
      final String CRLF = "\r\n"; // Line separator required by multipart/form-data.

      HttpURLConnection connection = null;
      OutputStream output = null;
      PrintWriter writer = null;
      try {
        connection = (HttpURLConnection) new URL( url ).openConnection();
        connection.setDoOutput( true ); // implicitly sets method to POST
        connection.setRequestProperty( "Content-Type", "multipart/form-data; boundary=" + boundary );
        output = connection.getOutputStream();
        writer = new PrintWriter( output );
        // Start of multipart/form-data.
        writer.append( "--" + boundary ).append( CRLF );
        writer.append( "Content-Disposition: form-data; name=\"file\"; filename=\"/" + targetFileName + "\"" ).append( CRLF );
        writer.append( "Content-Type: " + URLConnection.guessContentTypeFromName( targetFileName ) ).append( CRLF );
        if (data != null) {
          writer.append( CRLF ).flush();
          writer.append( data );
        }
        if (file != null) {
          writer.append( "Content-Transfer-Encoding: binary" ).append( CRLF );
          writer.append( CRLF ).flush();
          output.write( StreamHelper.toBytes( new FileInputStream( file ) ) );
        }
        output.flush(); // Important before continuing with writer!
        writer.append( CRLF ).flush(); // CRLF is important! It indicates end of boundary.

        // End of multipart/form-data.
        writer.append( "--" + boundary + "--" ).append( CRLF ).flush();

        // Request is lazily fired whenever you need to obtain information about response.
        responseCode = connection.getResponseCode();
      }
      finally {
        StreamHelper.close( writer );
        StreamHelper.close( output );
        if (connection != null) {
          connection.disconnect();
        }
      }
    }
    return responseCode;
  }

  private Model doLoadConfig( InetAddress inetAddress, String file, boolean optional ) {
    if (railDeviceAddress != null) {
      String url = "http://" + inetAddress.getHostAddress() + "/" + file;
      URI uri = URI.create( url );
      HttpURLConnection httpConnection = null;
      InputStream inStream = null;
      try {
        XmlReader xmlReader = new XmlReader();
        URL destination = uri.toURL();
        httpConnection = (HttpURLConnection) destination.openConnection();
        httpConnection.setRequestMethod( "GET" );
        inStream = httpConnection.getInputStream();
        return xmlReader.loadData( inStream, null );
      }
      catch (FileNotFoundException fex) {
        if (!optional) {
          Logger.error( "Config file not found at " + url, fex );
        }
      }
      catch (IOException e) {
        Logger.error( "Error loading config from "+url, e );
      }
      finally {
        StreamHelper.close( inStream );
        if (httpConnection != null) {
          httpConnection.disconnect();
        }
      }
    }
    return null;
  }

  public static ConfigLoader loadConfig( QName railDeviceId, InetAddress railDeviceAddress, String file, ConfigLoaderListener listener, boolean  tryIcon, boolean tryTrackCfg ) {
    return new ConfigLoader( railDeviceId, railDeviceAddress, file, listener, tryIcon, tryTrackCfg );
  }

  public void stop() {
    this.active = false;
    this.loaderThread.interrupt();
  }

  @Override
  public void run() {
    active = true;
    int retry = 3;
    while (active && (configModel == null) && (retry > 0)) {
      try {
        configModel = doLoadConfig( railDeviceAddress, filename, false );
      }
      catch (Exception ex) {
        Logger.error( "Error loading config for " + railDeviceAddress, ex );
      }
      retry--;
      if (configModel == null) {
        try {
          Thread.sleep( 1000 );
        }
        catch (InterruptedException e) {
          if (!active) return;
        }
      }
    }
    boolean success = (configModel != null);
    if (active && success) {
      if (tryIcon) {
        try {
          icon = loadBitmap( railDeviceAddress, "icon.jpg" );
        }
        catch (Exception ex) {
          Logger.error( "Error loading icon for " + railDeviceAddress );
        }
      }
      if (tryTrackCfg) {
        try {
          trackConfigModel = doLoadConfig( railDeviceAddress, TRACK_CFG_XML, true );
        }
        catch (Exception ex) {
          Logger.error( "Error loading config for " + railDeviceAddress, ex );
        }
      }
    }
    if (active) {
      listener.finishedLoading( this, success );
      active = false;
    }
  }
}
