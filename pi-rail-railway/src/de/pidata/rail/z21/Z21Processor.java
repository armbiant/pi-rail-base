package de.pidata.rail.z21;

import de.pidata.log.Logger;

import java.io.IOException;
import java.net.InetAddress;

import de.pidata.models.types.simple.DecimalObject;
import de.pidata.rail.model.MotorState;
import de.pidata.rail.railway.Locomotive;
import de.pidata.rail.railway.ModelRailway;
import de.pidata.rail.railway.RailAction;

public class Z21Processor implements Z21Interface {
  private Z21Comm z21Comm;
  private ModelRailway modelRailway;

  public Z21Processor( ModelRailway modelRailway ) {
    this.modelRailway = modelRailway;
    this.z21Comm = new Z21Comm( this );
  }

  public Z21Comm getZ21Comm() {
    return z21Comm;
  }

  public boolean start() {
    return this.z21Comm.start();
  }

  public void stop() {
    z21Comm.stop();
  }

  @Override
  public void notifyz21RailPower( int state ) {
    Logger.info( "TODO Z21: notifyz21RailPower=" + state );
  }

  @Override
  public void notifyz21EthSend( InetAddress client, int[] data ) {
    byte[] msgBytes = new byte[data.length];
    for (int i = 0; i < data.length; i++) {
      msgBytes[i] = (byte) data[i];
    }
    try {
      z21Comm.send( client, Z21Comm.Z21_PORT, msgBytes );
    }
    catch (IOException e) {
      Logger.error( "Z21 send error", e );
    }
  }

  @Override
  public void notifyz21S88Data( int data ) {
    Logger.info( "" );
    //z21.setS88Data (datasend);  //Send back state of S88 Feedback
  }

  @Override
  public void notifyz21getLocoState( int Adr, boolean bc ) {
    Logger.info( "TODO Z21: notifyz21getLocoState, bc="+bc );
    //void setLocoStateFull (int Adr, int steps, int speed, int F0, int F1, int F2, int F3, bool bc);
  }

  @Override
  /**
   * @return int-array: int Steps[0], int Speed[1], int F0[2], int F1[3], int F2[4], int F3[5]
   */
  public int[] requestZ21LocoState( int adr ) {
    Locomotive loco = modelRailway.getLoco( adr );
    int[] ldata = new int[6];
    ldata[0] = Z21Comm.DCCSTEP128;
    if (loco != null) {
      ldata[1] = (int) (loco.getTargetSpeedPercent().doubleValue() * 128 / 100);
      if (loco.getTargetDirChar() == MotorState.CHAR_BACK) {
        ldata[1] = ldata[1] + 128;
      }
      if (loco.getDCCFunctionState( 0 )) {
        ldata[2] = ldata[2] | 0x10;
      }
      int bit = 1;
      for (int i = 1; i <= 4; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[2] = ldata[2] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 5; i <= 12; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[3] = ldata[3] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 13; i <= 20; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[4] = ldata[4] | bit;
        }
        bit = bit << 1;
      }
      bit = 1;
      for (int i = 21; i <= 28; i++) {
        if (loco.getDCCFunctionState( i )) {
          ldata[5] = ldata[5] | bit;
        }
        bit = bit << 1;
      }
    }
    return ldata;
  }

  @Override
  public void setZ21LocoSpeed( int adr, int speed, int steps ) {
    Locomotive loco = modelRailway.getLoco( adr );
    if (loco == null) {
      Logger.info( "setZ21LocoSpeed loco for addr=" + adr + " not found" );
    }
    else {
      int dir = speed & 0x80;
      if (dir == 0) {
        loco.setTargetDir( MotorState.DIR_FORWARD );
      }
      else {
        speed = speed - 128;
        loco.setTargetDir( MotorState.DIR_BACK );
      }
      Logger.info( "setZ21LocoSpeed addr=" + adr + ",dir="+dir+", speed=" + speed + ", steps=" + steps );
      double speedPercent = ((double) speed) / steps * 100;
      loco.setTargetSpeedPercent( new DecimalObject( speedPercent, 0 ) );
    }
  }

  //--------------------------------------------------------------------------------------------

  /**
   *
   * @param adr  DCC loco address
   * @param type Type (0=AUS / 1=EIN / 2=UM)
   * @param fkt  function number (0-63)
   */
  @Override
  public void setZ21LocoFkt( int adr, int type, int fkt ) {
    Logger.info( "setZ21LocoFkt addr=" + adr + ", type=" + type + ", fkt=" + fkt );
    Locomotive loco = modelRailway.getLoco( adr );
    if (loco != null) {
      boolean newValue;
      if (type == 2) {
        newValue = !loco.getDCCFunctionState( fkt );
      }
      else {
        newValue = (type == 1);
      }
      loco.setDCCFunctionState( fkt, newValue );
    }
  }

  //--------------------------------------------------------------------------------------------

  @Override
  public void notifyz21Accessory( int addr, boolean state, boolean active ) {
    Logger.info( "notifyz21Accessory, adr=" + addr + ", state=" + state + ", active=" + active );
    RailAction railAction = modelRailway.getRailAction( addr );
    if ((railAction != null) && active) {
      railAction.setValueDCC( addr, state );
    }
  }

  @Override
  public boolean notifyz21AccessoryInfo( int addr ) {
    RailAction railAction = modelRailway.getRailAction( addr );
    if (railAction == null) {
      Logger.info( "notifyz21AccessoryInfo, addr="+addr+" unknown" );
      return false;
    }
    else {
      boolean value = railAction.getValueDCC( addr );
      Logger.info( "notifyz21AccessoryInfo, addr="+addr+", value="+value );
      return value;
    }
  }

  @Override
  public int notifyz21LNdispatch( int Adr2, int Adr ) {
    Logger.info( "TODO Z21: notifyz21LNdispatch" );
    //return the Slot that was dispatched, 0xFF at error!
    return 0xFF;
  }

  @Override
  public void notifyz21LNSendPacket( int[] data, int length ) {
    Logger.info( "TODO Z21: notifyz21LNSendPacket" );
  }

  @Override
  public void notifyz21CVREAD( int cvAdrMSB, int cvAdrLSB ) {
    Logger.info( "TODO Z21: notifyz21CVREAD" );
  }

  @Override
  public void notifyz21CVWRITE( int cvAdrMSB, int cvAdrLSB, int value ) {
    Logger.info( "TODO Z21: notifyz21CVWRITE" );
  }

  @Override
  public void notifyz21CVPOMWRITEBYTE( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMWRITEBYTE" );
  }

  /*@Override
  public int notifyz21ClientHash( InetAddress client ) {
    Logger.info( "TODO Z21: notifyz21ClientHash" );
    return 0;
  }*/

  @Override
  public void notifyz21CVPOMREADBYTE( int adr, int cvAdr ) {
    Logger.info( "TODO Z21: notifyz21CVPOMREADBYTE" );
  }

  @Override
  public void notifyz21CVPOMWRITEBIT( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMWRITEBIT" );
  }

  @Override
  public void notifyz21UpdateConf() {
    Logger.info( "TODO Z21: notifyz21UpdateConf" );
  }

  @Override
  public void notifyz21CANdetector( InetAddress client, int b, int word ) {
    Logger.info( "TODO Z21: notifyz21CANdetector" );
  }

  @Override
  public void notifyz21LNdetector( InetAddress client, int b, int word ) {
    Logger.info( "TODO Z21: notifyz21LNdetector" );
  }

  @Override
  public int notifyz21Railcom() {
    Logger.info( "TODO Z21: notifyz21Railcom" );
    return 0;
  }

  @Override
  public void notifyz21getSystemInfo( InetAddress client ) {
    Logger.info( "TODO Z21: notifyz21getSystemInfo client=" + client );
  }

  @Override
  public void notifyz21LocoFktExt( int word, int b, int b1 ) {
    Logger.info( "TODO Z21: notifyz21LocoFktExt word=" + word + ", b=" + b + ", b1=" + b1 );
  }

  @Override
  public void notifyz21LocoFkt61to68( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt61to68 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt53to60( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt53to60 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt45to52( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt45to52 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt37to44( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt37to44 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt29to36( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt29to36 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt21to28( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt21to28 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt13to20( int word, int b ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt13to20 word=" + word + ", b=" + b );
  }

  @Override
  public void notifyz21LocoFkt9to12( int word, int i ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt9to12 word=" + word + ", i=" + i );
  }

  @Override
  public void notifyz21LocoFkt5to8( int word, int i ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt5to8 word=" + word + ", i=" + i );
  }

  @Override
  public void notifyz21LocoFkt0to4( int word, int i ) {
    Logger.info( "TODO Z21: notifyz21LocoFkt0to4 word=" + word + ", i=" + i );
  }

  @Override
  public void notifyz21ExtAccessory( int i, int b ) {
    Logger.info( "TODO Z21: notifyz21ExtAccessory" );
  }

  @Override
  public void notifyz21CVPOMACCREADBYTE( int adr, int cvAdr ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCREADBYTE" );
  }

  @Override
  public void notifyz21CVPOMACCWRITEBIT( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCWRITEBIT" );
  }

  @Override
  public void notifyz21CVPOMACCWRITEBYTE( int adr, int cvAdr, int value ) {
    Logger.info( "TODO Z21: notifyz21CVPOMACCWRITEBYTE" );
  }

  public static void main( String[] args ) {
    Z21Processor z21 = null;
    try {
      z21 = new Z21Processor( new ModelRailway() );
      z21.getZ21Comm().start();
      while (true) {
        Thread.sleep( 1000 );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in Z21", ex );
    }
    finally {
      if (z21 != null) {
        z21.getZ21Comm().stop();
      }
    }
  }
}