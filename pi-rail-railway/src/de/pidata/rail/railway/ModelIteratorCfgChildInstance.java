package de.pidata.rail.railway;

import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelIterator;
import de.pidata.rail.model.Cfg;

import java.util.Iterator;

public class ModelIteratorCfgChildInstance<CM extends Model> implements ModelIterator<CM> {

  private ModelIterator<RailDevice> rootIterator;
  private RailDevice nextInRoot;
  private CM next;
  private Class iterClass;

  public ModelIteratorCfgChildInstance( ModelIterator<RailDevice> rootIterator, Class iterClass ) {
    this.rootIterator = rootIterator;
    this.iterClass = iterClass;
    if (rootIterator.hasNext()) {
      nextInRoot = rootIterator.next();
      if (nextInRoot != null) {
        findNext();
      }
    }
    else {
      nextInRoot = null;
    }
  }

  private void findNext() {
    Model child = next;
    do {
      if (child == null) {
        Cfg cfg = nextInRoot.getConfig();
        if (cfg != null) {
          child = cfg.firstChild( null );
        }
      }
      else {
        child = child.nextSibling( null );
      }
      while (child == null) {
        if (rootIterator.hasNext()) {
          nextInRoot = rootIterator.next();
          Cfg cfg = nextInRoot.getConfig();
          if (cfg != null) {
            child = cfg.firstChild( null );
          }
        }
        else {
          next = null;
          return;
        }
      }
    } while (!(iterClass.isAssignableFrom( child.getClass() )));
    next = (CM) child;
  }

  /**
   * Returns true if there is a further Model in this iteration, i.e. the next call to next()
   * will return a Model not null.
   *
   * @return true if there is a further Model in this iteration
   */
  @Override
  public boolean hasNext() {
    return (next != null);
  }

  /**
   * Returns the next Model of this iterator and sets the internal pointer to the next Model.
   *
   * @return the next Model of this iteration
   * @throws IllegalArgumentException if there is no next Model in this iterator
   */
  @Override
  public CM next() {
    CM current;
    if (next == null) {
      throw new IllegalArgumentException( "No more Elements in iterator." );
    }
    current = next;
    findNext();
    return current;
  }

  /**
   * Returns an iterator over elements of type {@code T}.
   *
   * @return an Iterator.
   */
  @Override
  public Iterator<CM> iterator() {
    return this;
  }

  public String toString() {
    return "ModelIteraterSubChildInstance iterClass="+iterClass+", next="+next;
  }
}
