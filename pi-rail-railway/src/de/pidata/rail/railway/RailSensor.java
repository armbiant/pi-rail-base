// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.railway;

import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.model.ActionState;
import de.pidata.rail.model.SensorAction;
import de.pidata.rail.model.State;
import java.lang.Integer;
import java.lang.String;
import java.util.Hashtable;

public class RailSensor extends RailAction {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/railway.xsd" );

  public static final QName ID_AGO = NAMESPACE.getQName("ago");
  public static final QName ID_NUM = NAMESPACE.getQName("num");
  public static final QName ID_READ = NAMESPACE.getQName("read");
  public static final QName ID_RECEIVETIME = NAMESPACE.getQName("receiveTime");
  public static final QName ID_TYPE = NAMESPACE.getQName("type");

  public RailSensor( Key id ) {
    super( id, RailwayFactory.RAILSENSOR_TYPE, null, null, null );
  }

  public RailSensor(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, RailwayFactory.RAILSENSOR_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected RailSensor(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute num.
   *
   * @return The attribute num
   */
  public Integer getNum() {
    return (Integer) get( ID_NUM );
  }

  /**
   * Set the attribute num.
   *
   * @param num new value for attribute num
   */
  public void setNum( Integer num ) {
    set( ID_NUM, num );
  }

  /**
   * Returns the attribute type.
   *
   * @return The attribute type
   */
  public String getType() {
    return (String) get( ID_TYPE );
  }

  /**
   * Set the attribute type.
   *
   * @param type new value for attribute type
   */
  public void setType( String type ) {
    set( ID_TYPE, type );
  }

  /**
   * Returns the attribute read.
   *
   * @return The attribute read
   */
  public String getRead() {
    return (String) get( ID_READ );
  }

  /**
   * Set the attribute read.
   *
   * @param read new value for attribute read
   */
  public void setRead( String read ) {
    set( ID_READ, read );
  }

  /**
   * Returns the attribute ago.
   *
   * @return The attribute ago
   */
  public Integer getAgo() {
    return (Integer) get( ID_AGO );
  }

  /**
   * Set the attribute ago.
   *
   * @param ago new value for attribute ago
   */
  public void setAgo( Integer ago ) {
    set( ID_AGO, ago );
  }

  /**
   * Returns the attribute receiveTime.
   *
   * @return The attribute receiveTime
   */
  public DateObject getReceiveTime() {
    return (DateObject) get( ID_RECEIVETIME );
  }

  /**
   * Set the attribute receiveTime.
   *
   * @param receiveTime new value for attribute receiveTime
   */
  public void setReceiveTime( DateObject receiveTime ) {
    set( ID_RECEIVETIME, receiveTime );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public RailSensor( SensorAction config, RailDevice railDevice ) {
    this( config.getId() );
    init( config, railDevice );
    setAgo( -1 );
    setRead( "new" );
  }

  public QName getCurrentImageID( boolean diag ) {
    String iconName = "sensor_straight_active";
    if (diag) {
      iconName += "_diag";
    }
    //FIXME: check whether file exists, otherwise return default Image
    return NAMESPACE.getQName( "icons/actions/Sensor/" + iconName + ".png" );
  }

  /**
   * Called by this RailAction's device after having received a Event or State containing ev
   *
   * @param sensorState          the ev received from our railDevice
   * @param receiveTime the time the message was received by PIRailComm
   */
  @Override
  public void processEvent( ActionState sensorState, long receiveTime ) {
    super.processEvent( sensorState, receiveTime );
    State stateMsg = (State) sensorState.getParent( false );
    setAgo( (int) (stateMsg.getMsLong() - sensorState.getUpdLong()) );
    QName srcID = sensorState.getSrcID();
    if (srcID == null) {
      setRead( "" );
    }
    else {
      setRead( sensorState.getSrcID().getName() );
      RailAction railMessage = PiRail.getInstance().getModelRailway().addOrGetMessage( srcID );
      if (railMessage instanceof RailMessage) {
        ((RailMessage) railMessage).setLastReceived( getDeviceID(), sensorState.getUpdLong(), new DateObject( DateTimeType.TYPE_DATETIME, receiveTime ) );
      }
    }
    setReceiveTime( new DateObject( DateTimeType.TYPE_TIME, System.currentTimeMillis() ) );
  }

  @Override
  public void processState( ActionState actionState, long receiveTime ) {
    this.actionState = actionState;
    Integer val = actionState.getCurI();
    if (val == null) val = 0;
    Integer num = getNum();
    boolean inital = (num == null);
    if (!val.equals( num )) {
      setNum( val );
      QName srcID = actionState.getSrcID();
      if (srcID == null) {
        setRead( actionState.getCur() );
      }
      else {
        setRead( srcID.getName() );
        RailAction railMessage = PiRail.getInstance().getModelRailway().addOrGetMessage( srcID );
        if (railMessage instanceof RailMessage) {
          ((RailMessage) railMessage).setLastReceived( getDeviceID(), actionState.getUpdLong(), new DateObject( DateTimeType.TYPE_DATETIME, receiveTime ) );
        }
      }
      if (!inital) {
        PiRail.getInstance().getModelRailway().stateChanged( this, actionState );
      }
    }
  }

  @Override
  public void setValue( char newValue, int newIntValue, QName lockID ) {
    throw new IllegalArgumentException( "Cannot set RailSensor's value" );
  }

  @Override
  public int stateCount() {
    return 0;
  }

  @Override
  public QName getIconState( int index, boolean diag ) {
    return null;
  }

  @Override
  public void invokeAction( int index ) {
    // no actions
  }
}
