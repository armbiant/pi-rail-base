// generated by ClassGenerator, (C) PI-Data AG 2005, (http://www.pi-data.de)

package de.pidata.rail.railway;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.log.Logger;
import de.pidata.models.tree.*;
import de.pidata.models.tree.ChildList;
import de.pidata.models.types.ComplexType;
import de.pidata.models.types.complex.DefaultComplexType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.Key;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.rail.comm.PiRail;
import de.pidata.rail.comm.ScriptRunner;
import de.pidata.rail.comm.WifiState;
import de.pidata.rail.model.*;
import de.pidata.string.Helper;
import java.lang.Long;
import java.util.Hashtable;

public abstract class RailAction extends de.pidata.models.tree.SequenceModel {

  public static final Namespace NAMESPACE = Namespace.getInstance( "http://res.pirail.org/railway.xsd" );

  public static final QName ID_ID = NAMESPACE.getQName("id");
  public static final QName ID_LASTCHANGE = NAMESPACE.getQName("lastChange");
  public static final QName ID_WLANICON = NAMESPACE.getQName("wlanicon");

  public RailAction( Key id ) {
    super( id, RailwayFactory.RAILACTION_TYPE, null, null, null );
  }

  public RailAction(Key key, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, RailwayFactory.RAILACTION_TYPE, attributeNames, anyAttribs, childNames);
  }

  protected RailAction(Key key, ComplexType type, Object[] attributeNames, Hashtable anyAttribs, ChildList childNames) {
    super(key, type, attributeNames, anyAttribs, childNames);
  }


  /**
   * Returns the attribute id.
   *
   * @return The attribute id
   */
  public QName getId() {
    return (QName) get( ID_ID );
  }

  /**
   * Returns the attribute wlanicon.
   *
   * @return The attribute wlanicon
   */
  public QName getWlanicon() {
    return (QName) get( ID_WLANICON );
  }

  /**
   * Set the attribute wlanicon.
   *
   * @param wlanicon new value for attribute wlanicon
   */
  public void setWlanicon( QName wlanicon ) {
    set( ID_WLANICON, wlanicon );
  }

  /**
   * Returns the attribute lastChange.
   *
   * @return The attribute lastChange
   */
  public Long getLastChange() {
    return (Long) get( ID_LASTCHANGE );
  }

  /**
   * Set the attribute lastChange.
   *
   * @param lastChange new value for attribute lastChange
   */
  public void setLastChange( Long lastChange ) {
    set( ID_LASTCHANGE, lastChange );
  }

  //----------------------------------------------------------
  // Manuelle Erweiterungen

  public static final QName ID_DEVICENAME = NAMESPACE.getQName( "deviceName" );
  public static final QName ID_STATEVALUE = NAMESPACE.getQName( "stateValue" );
  public static final QName ID_CURRENT_ICON = NAMESPACE.getQName( "currentIcon" );
  public static final QName ID_ICON_STATE_1 = NAMESPACE.getQName( "iconState1" );
  public static final QName ID_ICON_STATE_2 = NAMESPACE.getQName( "iconState2" );
  public static final QName ID_ICON_STATE_3 = NAMESPACE.getQName( "iconState3" );
  public static final QName ID_ICON_STATE_4 = NAMESPACE.getQName( "iconState4" );
  public static final QName ID_ICON_STATE_5 = NAMESPACE.getQName( "iconState5" );
  public static final QName ID_ACTION = NAMESPACE.getQName( "action" );
  public static ComplexType TRANSIENT_TYPE;

  protected Action action;
  protected ActionState actionState;
  protected RailDevice railDevice;
  private QName lockID = null; // only valid if railDevice is null

  public RailAction( Action action, RailDevice railDevice ) {
    this( action.getId() );
    init( action, railDevice );
  }

  public void setRailDevice( RailDevice railDevice ) {
    if (this.railDevice == null) {
      this.railDevice = railDevice;
    }
    else {
      throw new IllegalArgumentException( "Must not change Action's RailDevice" );
    }
  }

  @Override
  protected void initTransient() {
    super.initTransient();
  }

  @Override
  public ComplexType transientType() {
    if (TRANSIENT_TYPE == null) {
      DefaultComplexType type = new DefaultComplexType( RailFunction.NAMESPACE.getQName( "Turnout_Transient" ), RailFunction.class.getName(), 0 );
      TRANSIENT_TYPE = type;
      type.addAttributeType( ID_DEVICENAME, StringType.getDefString() );
      type.addAttributeType( ID_STATEVALUE, StringType.getDefString() );
      type.addAttributeType( ID_CURRENT_ICON, QNameType.getQName() );
      type.addAttributeType( ID_ICON_STATE_1, QNameType.getQName() );
      type.addAttributeType( ID_ICON_STATE_2, QNameType.getQName() );
      type.addAttributeType( ID_ICON_STATE_3, QNameType.getQName() );
      type.addAttributeType( ID_ICON_STATE_4, QNameType.getQName() );
      type.addAttributeType( ID_ICON_STATE_5, QNameType.getQName() );
      type.addRelation( Action.ID_TRACKPOS, PiRailFactory.TRACKPOS_TYPE, 0, 1 );
      type.addRelation( ID_ACTION, PiRailFactory.ACTION_TYPE, 0, 1 );
    }
    return TRANSIENT_TYPE;
  }

  @Override
  public Model clone( Key newKey, boolean deep, boolean linked ) {
    RailAction clone = (RailAction) super.clone( newKey, deep, linked );
    clone.init( action, railDevice );
    return clone;
  }

  protected void init( Action actionCfg, RailDevice railDevice ) {
    this.action = actionCfg;
    this.railDevice = railDevice;
  }

  public RailDevice getRailDevice() {
    return railDevice;
  }

  public QName getDeviceID() {
    if (railDevice == null) {
      return null;
    }
    else {
      return railDevice.getId();
    }
  }

  public Action getAction() {
    return action;
  }

  public ActionState getActionState() {
    return actionState;
  }

  public ItemConn getItemConn() {
    if (action == null) {
      return null;
    }
    else {
      return action.getItemConn();
    }
  }

  public QName getItemConnID() {
    ItemConn itemConn = getItemConn();
    if (itemConn == null) {
      return null;
    }
    else {
      return itemConn.getId();
    }
  }

  public void setLockID( QName lockID ) {
    if (railDevice == null) {
      actionState.setLck( lockID );
    }
  }

  public QName getLockID() {
    if (actionState == null) {
      return null;
    }
    else {
      return actionState.getLck();
    }
  }

  public TrackPos getTrackPos() {
    if (action == null) {
      return null;
    }
    else {
      return action.getTrackPos();
    }
  }

  protected void updateWlanIcon() {
    if (railDevice == null) {
      setWlanicon( null );
    }
    else {
      QName oldIcon = getWlanicon();
      WifiState wifiState = railDevice.getWifiState();
      QName newIcon = null;
      switch (wifiState) {
        case green: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_green_black.png" ); break;
        case green_bad: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_green_bad.png" ); break;
        case green_ok: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_green_ok.png" ); break;
        case green_good: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_green_good.png" ); break;
        case green_top: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_green_top.png" ); break;
        case yellow: newIcon = RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_yellow_black.png" ); break;
        default: newIcon =  RailDevice.NAMESPACE_GUI.getQName( "icons/wifi/wlan_edit_red_black.png" );
      }
      if (newIcon != oldIcon) {
        setWlanicon( newIcon );
      }
    }
  }

  public abstract int stateCount();

  public abstract QName getIconState( int index, boolean diag );

  public QName getCurrentImageID( boolean diag ) {
    QName imageID = GuiBuilder.NAMESPACE.getQName( "icons/actions/Action_start.png" );
    return imageID;
  }

  @Override
  public Object transientGet( int transientIndex ) {
    QName attributeName = transientType().getAttributeName( transientIndex );
    if (attributeName == ID_STATEVALUE) {
      return getCurrentValue();
    }
    else if (attributeName == ID_DEVICENAME) {
      if (railDevice == null) {
        if (action instanceof TrackMsg) {
          return "Tag "+action.getId().getName();
        }
        else {
          return null;
        }
      }
      else {
        return railDevice.getDisplayName();
      }
    }
    else if (attributeName == ID_CURRENT_ICON) {
      return getCurrentImageID( false );
    }
    else if (attributeName == ID_ICON_STATE_1) {
      return getIconState( 0, false );
    }
    else if (attributeName == ID_ICON_STATE_2) {
      return getIconState( 1, false );
    }
    else if (attributeName == ID_ICON_STATE_3) {
      return getIconState( 2, false );
    }
    else if (attributeName == ID_ICON_STATE_4) {
      return getIconState( 3, false );
    }
    else if (attributeName == ID_ICON_STATE_5) {
      return getIconState( 4, false );
    }
    else {
      return super.transientGet( transientIndex );
    }
  }

  @Override
  public ModelIterator transientChildIter( QName relationName, Filter filter ) {
    if (relationName == Action.ID_TRACKPOS) {
      return new ModelIteratorSingle<TrackPos>( getTrackPos() );
    }
    else if (relationName == ID_ACTION) {
      return new ModelIteratorSingle<Action>( getAction() );
    }
    else {
      return super.transientChildIter( relationName, filter );
    }
  }

  protected void fireIconsChanged() {
    fireDataChanged( ID_CURRENT_ICON, null, getCurrentImageID( false ) );
    fireDataChanged( ID_ICON_STATE_1, null, getIconState( 0, false ) );
    fireDataChanged( ID_ICON_STATE_2, null, getIconState( 1, false ) );
    fireDataChanged( ID_ICON_STATE_3, null, getIconState( 2, false ) );
    fireDataChanged( ID_ICON_STATE_4, null, getIconState( 3, false ) );
    fireDataChanged( ID_ICON_STATE_5, null, getIconState( 4, false ) );
  }

  public String getCurrentValue () {
    if (actionState == null) {
      return null;
    }
    else {
      return actionState.getCur();
    }
  }

  public char getCurrentChar() {
    String cur = getCurrentValue();
    if (Helper.isNullOrEmpty( cur )) {
      return 0;
    }
    else {
      return cur.charAt( 0 );
    }
  }

  public boolean isActiveValue( char value ) {
    return (getCurrentChar() == value);
  }

  public abstract void processState( ActionState actionState, long receiveTime );

  /**
   * Called by this RailAction's device after having received a Event or State containing ev
   * @param sensorState          the ev received from our railDevice
   * @param receiveTime the time the message was received by PIRailComm
   */
  public void processEvent( ActionState sensorState, long receiveTime ) {
    Logger.info( "Received event state, id=" + sensorState.getId() + ", srcID='" + sensorState.getSrcID() + "', upd=" + sensorState.getUpd()  );
    QName srcID = sensorState.getSrcID();
    if (srcID != null) {
      RailAction railAction = PiRail.getInstance().getModelRailway().getRailAction( null, srcID );
      if (railAction instanceof RailTimer) {
        ((RailTimer) railAction).receivedEvent( railDevice, sensorState, receiveTime );
      }
    }
    processState( sensorState, receiveTime );
  }

  public abstract void setValue( char newValue, int newIntValue, QName lockID );

  public abstract void invokeAction( int index );

  public boolean execute( ScriptRunner scriptRunner, char value, int intVal, String indent ) throws InterruptedException {
    return false;
  }

  public int getDCCAddr() {
    ItemConn itemConn = getItemConn();
    if (itemConn != null) {
      Param param = itemConn.getParam( Param.PARAM_DCC_ADDR );
      if (param != null) {
        return param.getInt();
      }
    }
    return -1;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + " " + getId().getName();
  }

  public void setValueDCC( int addr, boolean state ) {
    // do nothing - overridden if applicable
  }

  public boolean getValueDCC( int addr ) {
    return false;
  }
}
