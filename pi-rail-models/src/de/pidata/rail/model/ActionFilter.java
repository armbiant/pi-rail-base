package de.pidata.rail.model;

import de.pidata.models.tree.Filter;
import de.pidata.models.tree.FilterListener;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;

public class ActionFilter implements Filter {

  @Override
  public void init( ModelSource dataSource ) {
    // do nothing
  }

  /**
   * Returns true if model matches this filter
   *
   * @param model the model to be matched
   * @return true if model matches this filter
   */
  @Override
  public boolean matches( Model model ) {
    return (model instanceof Action);
  }

  @Override
  public void setFilterListener( FilterListener filterListener ) {
    //do nothing
  }
}
